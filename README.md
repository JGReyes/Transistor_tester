Modificación del firmware de un comprobador de componentes basado en el trabajo
de Karl-Heinz Kübbeler. El proyecto original está en los foros de [mickocontroler.net](https://www.mikrocontroller.net/articles/AVR_Transistortester#Introduction_.28English.29)

Se ha cambiado para soportar una pantalla GLCD de 128x64 con chip KS7108 y se han
activado ciertas opciones adicionales, con el fin de sustituir la pantalla original
que se rompió.

![transistor_tester](https://1.bp.blogspot.com/-lfWHckAvFK8/WzStQ5oayAI/AAAAAAAABGw/mhO1Cf3mYNMPMesDihO3eJhe7O2uT904gCKgBGAs/s1600/20171028_0001.jpg)

Para más información visitar el [blog](https://www.circuiteando.net/2018/06/arreglo-comprobador-de-componentes.html)